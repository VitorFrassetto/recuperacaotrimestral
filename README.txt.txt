Nome:Vitor Jacinto Frassetto
Turma:3-52

1-As estruturas de programa��o das linguagens permitem criar logicas para o fluxo dos dados de nossos softwares. Assinale a alternativa que cont�m uma estrutura de programa��o usada pela linguagem Object Pascal que permite iterar indefinidamente sobre um mesmo conjunto de instru��es.
Resposta: C=While;
JUSTIFICATIVA:Pois o While continua repetindo at� que encontre algo que fa�a-o parar

2-As estruturas de dados presentes nas linguagens de programa��o s�o necess�rios que a aplica��o possa entender e armazenar corretamente os dados que pretendemos utilizar. Assinale a alternativa que cont�m, respectivamente, uma estrutura capaz de armazenar apenas n�meros inteiros positivos e negativos e uma estrutura que armazena apenas dois valores:
Resposta: A=Integer e Boolean;
JUSTIFICATIVA:pois integer n�o aceita letras nem caracteres especiais e boolean aceita so dois valor (0 ou 1, verdadeiro ou falso).

3- V�rias linguagens de programa��o permitem a cria��o de enumeradores. Este tipo de estrutura de dados permite criar uma vari�vel que armazena um valor pr�-definido pelo pr�prio programador. Utilizando como refer�ncia a linguagem Object-Pascal, assinale a alternativa que representa a cria��o de um enumerador para os dias da semana:
Resposta: C=type diaSemana = (seg , ter, qua, qui, sex );
JUSTIFICATIVA:Sintaxe correta.

4- Existe uma estrutura definida pelo Object Pascal capaz de agrupar itens de dados de diferentes tipos (ao contr�rio de array, que armazena v�rios itens do mesmo tipo ). Assinale a alternativa que representa o nome desta estrutura.
Resposta: B=Record;
JUSTIFICATIVA:aceita valores diferentes "String, integer"

5- O Git � um sistema de controle de vers�es desenvolvido por Linus Torvalds e Junio Hamano que facilita o processo de desenvolvimento de software ao ser usado para registrar a hist�ria de edi��es dos arquivos-fonte. Assinale a alternativa que n�o representa uma fun��o do Git:
Resposta: D=impedir o acesso n�o autorizado aos arquivos fonte por meio da autentica��o de usu�rio;
JUSTIFICATIVA:o git n�o impede que ninguem mexa mo projeto, e sim o na hora de salvar o projeto de onde ele clonou ou seja o git lab que impede.

6- Para organizar a �rea de desenvolvimento, o Git implementa diversas �reas com diferentes caracter�sticas dentro de um projeto. Dessa forma, o Git minimiza altera��es desastrosas que podem comprometer a integridade do c�digo. Assinale a alternativa que representa o nome da �rea do Git onde ficam armazenados os arquivos que est�o prontos para serem preservados permanentemente no reposit�rio local (por�m ainda n�o foram): 
Resposta: B=Stage/Index;
JUSTIFICATIVA:pois � a area de prepara��o que armazena as informa��es.

7- Ao realizar altera��es no c�digo, o git n�o preserva automaticamente as altera��es efetuadas em seu reposit�rio. Antes de mais nada, � necess�rio indicar ao versionador quais s�o as modifica��es que pretendemos preservar em uma nova vers�o. Assinale a alternativa que preserva APENAS as altera��es realizadas no arquivo Stark.php (levando em considera��o que o arquivo do mesmo projeto vingadores.php tamb�m possui altera��es);
Resposta: C=Git add stark.php;;
JUSTIFICATIVA:pois adiciona apenas o stark.php para ser commitado e deixa vingadores de "fora".

8- Quando h� modifica��es listadas no index do reposit�rio Git, podemos preservar permanentemente estas altera��es em um processo chamado de commit. Assinale a alternativa que melhor descreve o comportamento do comando git commit;
Resposta: A=Cria-se um identificador �nico para o commit e as modifica��es s�o preservadas no reposit�rio local;
JUSTIFICATIVA:

9- O reposit�rio remoto � uma defini��o do Git para uma c�pia remota do reposit�rio local de um determinado projeto. No entanto, estes dois tipos de reposit�rio podem conter diferen�as entre si, que necessitam da atualiza��o do programador. Assinale a alternativa que representa, respectivamente, o comando utilizado para atualizar o reposit�rio local (com o conte�do remoto) e o comando utilizado para atualizar o reposit�rio remoto ( com o conte�do local).
Resposta: E=GIT PULL e GIT PUSH;
JUSTIFICATIVA:Este comando realiza o "commit", pega os arquivos que est�o no index (que o git add adicionou) e cria uma revis�o com numeros e um comentario.

10- Os gerenciadores de reposit�rio baseados em Git (como o GitLab) s�o respons�veis por permitir em desenvolvedores armazenem remotamente seus reposit�rios e oferecem ferramentas para a constru��o do software de maneira colaborativa. Assinale a alternativa que apresenta o comando utilizado para realizar a c�pia de um reposit�rio para a sua m�quina, levando em considera��o:
Servidor:gitlab.com
Usu�rio: rvenson
Reposit�rio: prova01
Resposta: B=git clone https://gitlab.com/rvenson/prova01
JUSTIFICATIVA:pois o git clone � usado para clonar um projeto para sua maquina pela primeira vez.



